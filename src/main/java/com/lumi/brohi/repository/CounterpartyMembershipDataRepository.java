package com.lumi.brohi.repository;

import com.lumi.brohi.domain.converter.MembershipData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CounterpartyMembershipDataRepository extends JpaRepository<MembershipData, String> {

    List<MembershipData>  findAllByMembershipMarketCode(String membershipMarketCode);
    Optional<MembershipData> findByMembershipMarketCodeAndMembershipMemberCode(String membershipMarketCode, String membershipMemberCode);
    List<MembershipData> findAllByMembershipMemberIsSg(boolean membershipMemberIsSg);
}
