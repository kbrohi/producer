package com.lumi.brohi.domain.converter;

import com.lumi.brohi.domain.BaseEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="MEMBERSHIP_DATA")
@ToString
public class MembershipData extends BaseEntity {

    @Id
    @Column(name = "memberId")
    private String memberId;
    @Column(name = "market")
    private String market;
    @Column(name = "member")
    private String member;
    @Column(name = "member_first_name")
    private String memberFirstName;
    @Column(name = "member_last_name")
    private String memberLastName;
    @Column(name = "member_is")
    private Boolean memberIs;

}
