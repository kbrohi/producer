package com.lumi.brohi.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@MappedSuperclass
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1l;
    @Column(name = "last_update_by")
    protected String lastUpdateBy;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_update_time")
    protected Date lastUpdateTime;

}
