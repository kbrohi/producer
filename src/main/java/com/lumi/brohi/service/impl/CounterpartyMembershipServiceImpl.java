package com.lumi.brohi.service.impl;

import com.lumi.brohi.domain.converter.MembershipData;
import com.lumi.brohi.repository.CounterpartyMembershipDataRepository;
import com.lumi.brohi.service.CounterpartyMembershipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

@Service
public class CounterpartyMembershipServiceImpl implements CounterpartyMembershipService {


    @Autowired
    private CounterpartyMembershipDataRepository membershipDataRepository;
    @Override
    public List<MembershipData> findAllByMembershipMarketCode(String membershipMarketCode) {
        return membershipDataRepository.findAllByMembershipMarketCode(membershipMarketCode);
    }

    @Override
    public MembershipData findByMembershipMarketCodeAndMembershipMemberCode(String membershipMarketCode, String membershipMemberCode) {
        Optional<MembershipData> counterpartyMembershipDataOpt = membershipDataRepository.findByMembershipMarketCodeAndMembershipMemberCode(membershipMarketCode,membershipMemberCode);
        if(counterpartyMembershipDataOpt.isPresent()){
            return counterpartyMembershipDataOpt.get();
        }
        return null;
    }

    @Override
    public MembershipData saveCounterpartyMembership(MembershipData membershipData) {
        Optional<MembershipData> counterpartyMembershipDataOpt = membershipDataRepository.findByMembershipMarketCodeAndMembershipMemberCode(
                membershipData.getMembershipMarketCode(), membershipData.getMembershipMemberCode());
        if(counterpartyMembershipDataOpt.isPresent()){
            MembershipData membershipDataDB = counterpartyMembershipDataOpt.get();
            if(membershipData.getMembershipMemberIsSg() !=null)
                membershipDataDB.setMembershipMemberIsSg(membershipData.getMembershipMemberIsSg());
            if(StringUtils.hasLength(membershipData.getMembershipMemberLongName()))
                membershipDataDB.setMembershipMemberLongName(membershipData.getMembershipMemberLongName());
            if(StringUtils.hasLength(membershipData.getMembershipMemberShortName()))
                membershipDataDB.setMembershipMemberShortName(membershipData.getMembershipMemberShortName());
            return membershipDataRepository.save(membershipDataDB);
        }else{
            return membershipDataRepository.save(membershipData);

        }

    }

    @Override
    public MembershipData updateCounterpartyMembership(String membershipMarketCode, String memberCode, MembershipData membershipData) {
        Optional<MembershipData> counterpartyMembershipDataOpt = membershipDataRepository.findByMembershipMarketCodeAndMembershipMemberCode(membershipMarketCode,memberCode);
        if(counterpartyMembershipDataOpt.isPresent()){
            MembershipData membershipDataDB = counterpartyMembershipDataOpt.get();
            if(membershipData.getMembershipMemberIsSg() !=null)
                membershipDataDB.setMembershipMemberIsSg(membershipData.getMembershipMemberIsSg());
            if(StringUtils.hasLength(membershipData.getMembershipMemberLongName()))
                membershipDataDB.setMembershipMemberLongName(membershipData.getMembershipMemberLongName());
            if(StringUtils.hasLength(membershipData.getMembershipMemberShortName()))
                membershipDataDB.setMembershipMemberShortName(membershipData.getMembershipMemberShortName());
            return membershipDataRepository.save(membershipDataDB);
        }

        return null;
    }

    @Override
    public MembershipData deleteCounterpartyMembership(String membershipMarketCode, String memberCode) {
        Optional<MembershipData> counterpartyMembershipDataOpt = membershipDataRepository.findByMembershipMarketCodeAndMembershipMemberCode(membershipMarketCode,memberCode);
        if(counterpartyMembershipDataOpt.isPresent()){
            membershipDataRepository.deleteById(counterpartyMembershipDataOpt.get().getMembershipMemberLei());
            return counterpartyMembershipDataOpt.get();
        }
        return null;
    }
}
