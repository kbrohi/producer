package com.lumi.brohi.service;

import com.lumi.brohi.domain.converter.MembershipData;

import java.util.List;


public interface CounterpartyMembershipService {
    List<MembershipData> findAllByMembershipMarketCode(String membershipMarketCode);
    MembershipData findByMembershipMarketCodeAndMembershipMemberCode(String membershipMarketCode, String membershipMemberCode);
    MembershipData saveCounterpartyMembership(MembershipData membershipData);
    MembershipData updateCounterpartyMembership(String membershipMarketCode, String memberCode, MembershipData membershipData);
    MembershipData deleteCounterpartyMembership(String membershipMarketCode, String memberCode);

}
