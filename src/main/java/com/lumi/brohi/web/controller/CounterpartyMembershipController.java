package com.lumi.brohi.web.controller;

import com.lumi.brohi.domain.converter.MembershipData;
import com.lumi.brohi.service.CounterpartyMembershipService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/membership")
@Slf4j
public class CounterpartyMembershipController {

    @Autowired
    private CounterpartyMembershipService counterpartyMembershipService;
    @GetMapping("/testing")
    public ResponseEntity<?> testing() {
        log.info("testing Start");
        log.info("testing completed");
        return ResponseEntity.ok("testing");
    }
    @PostMapping("")
    public ResponseEntity<?> saveMembershipListByMarketCodeAndMemberCode(@RequestBody MembershipData membershipData) {
        log.info("saveMembershipListByMarketCodeAndMemberCode Start {}", membershipData);

        membershipData = counterpartyMembershipService.saveCounterpartyMembership(membershipData);
        log.info("saveMembershipListByMarketCodeAndMemberCode completed {}", membershipData);
        return ResponseEntity.ok(membershipData);
    }

    @GetMapping("/{marketCode}")
    public ResponseEntity<?> getMembershipListByMarketCode(@PathVariable("marketCode") String marketCode) {
        log.info("getMembershipListByMarketCode Start {}", marketCode);
        List<MembershipData> membershipDataList = counterpartyMembershipService.findAllByMembershipMarketCode(marketCode);
        log.info("getMembershipListByMarketCode completed");
        log.debug("{}", membershipDataList);
        return ResponseEntity.ok(membershipDataList);
    }

    @GetMapping("/{marketCode}/{memberCode}")
    public ResponseEntity<?> getMembershipListByMarketCodeAndMemberCode(@PathVariable("marketCode") String marketCode, @PathVariable("memberCode") String memberCode) {
        log.info("getMembershipListByMarketCodeAndMemberCode Start {} , {}", marketCode, memberCode);
        MembershipData membershipData = counterpartyMembershipService.
                findByMembershipMarketCodeAndMembershipMemberCode(marketCode, memberCode);
        log.info("getMembershipListByMarketCodeAndMemberCode completed");
        log.debug("{}", membershipData);
        if (membershipData != null)
            return ResponseEntity.ok(membershipData);
        else
            return ResponseEntity.notFound().eTag("Membership Not Found against marketCode[" + marketCode + "], memberCode[" + memberCode + "]").build();
    }

    @PutMapping("/{marketCode}/{memberCode}")
    public ResponseEntity<?> putMembershipByMarketCodeAndMemberCode(@PathVariable("marketCode") String marketCode,
                                                                    @PathVariable("memberCode") String memberCode,
                                                                    @RequestBody MembershipData membershipData) {
        log.info("putMembershipByMarketCodeAndMemberCode Start {} , {} ", marketCode, memberCode);
        log.debug("data received {}", membershipData);
        membershipData = counterpartyMembershipService.updateCounterpartyMembership(marketCode, memberCode, membershipData);
        log.info("putMembershipByMarketCodeAndMemberCode completed");
        log.debug("data updated {}", membershipData);
        if (membershipData != null)
            return ResponseEntity.ok(membershipData);
        else
            return ResponseEntity.notFound().eTag("Membership Not Found against marketCode[" + marketCode + "], memberCode[" + memberCode + "]").build();
    }

    @DeleteMapping("/{marketCode}/{memberCode}")
    public ResponseEntity<?> deleteMembershipListByMarketCodeAndMemberCode(@PathVariable("marketCode") String marketCode,
                                                                           @PathVariable("memberCode") String memberCode) {
        log.info("deleteMembershipListByMarketCodeAndMemberCode Start {} , {} ", marketCode, memberCode);
        MembershipData membershipData = counterpartyMembershipService.deleteCounterpartyMembership(marketCode, memberCode);
        log.info("deleteMembershipListByMarketCodeAndMemberCode completed");
        log.debug("data {}", membershipData);
        if (membershipData != null)
            return ResponseEntity.accepted().body(membershipData);
        else
            return ResponseEntity.notFound().eTag("Membership Not Found against marketCode[" + marketCode + "], memberCode[" + memberCode + "]").build();
    }
}
